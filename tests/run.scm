(load-relative "../buffered-ports.scm")
(import buffered-ports)
(use test utils)

(test-begin)

(define (buf-port s n)
  (make-buffered-input-port (open-input-string s) n))

(define (buf-port* s n)
  (let ((chars (string->list s)))
    (make-buffered-input-port (make-input-port (lambda ()
                                                 (if (null? chars)
                                                     #!eof
                                                     (let ((char (car chars)))
                                                       (set! chars (cdr chars))
                                                       char)))
                                               (lambda ()
                                                 #t)
                                               (lambda ()
                                                 (set! chars #f)))
                              n)))

(define (chunk-port chunks)
  (let* ((chunks chunks)
         (pos 0))
    (make-buffered-input-port
     (make-input-port (lambda ()
                        (let loop ()
                          (if (null? chunks)
                              #!eof
                              (let ((chunk (car chunks)))
                                (if (= pos (string-length chunk))
                                    (begin
                                      (set! chunks (cdr chunks))
                                      (set! pos 0)
                                      (loop))
                                    (let ((old-pos pos))
                                      (set! pos (+ 1 pos))
                                      (string-ref chunk old-pos)))))))
                      (lambda ()
                        #t)
                      (lambda ()
                        (set! chunks #f))
                      (lambda ()
                        (let loop ((pos pos)
                                   (chunks chunks))
                          (if (null? chunks)
                              #!eof
                              (let ((chunk (car chunks)))
                                (if (= pos (string-length chunk))
                                    (loop 0 (cdr chunks))
                                    (string-ref chunk pos))))))
                      #f
                      #f
                      (lambda (port)
                        (if (null? chunks)
                            ""
                            (let ((chunk (substring (car chunks) pos)))
                              (set! chunks (cdr chunks))
                              chunk))))
     1)))

(test "hey" (read-all (buf-port* "hey" 1)))
(test "foo" (read-line (buf-port* "foo\nbar" 10)))

(let ((p (buf-port* "chicken" 2)))
  (test #\c (peek-char p))
  (test "c" (read-buffered p))
  (test "" (read-buffered p))
  (test #\h (read-char p)))

(test "foobarbaz" (read-all (chunk-port '("foo" "ba" "rbaz"))))

(let ((p (buf-port* "a" 1)))
  (peek-char p)
  (test-assert(char-ready? p))
  (test #\a (read-char p))
  (peek-char p)
  (test-assert (char-ready? p))
  (test-assert (eof-object? (read-char p))))

(for-each
 (lambda (chunks)
   (test-group (string-append "(read-line 1) with " (with-output-to-string (cut write chunks)))
     (let ((in (chunk-port chunks)))
       (test "a" (read-line in 1))
       (test "" (read-line in 1))
       (test "b" (read-line in 1))
       (test "" (read-line in 1)))))
 '(("a\nb\n")
   ("a" "\nb\n")))


(test "a" (read-line (chunk-port '("aa\n")) 1))
(test "a" (read-line (chunk-port '("aa")) 1))
(test "aa" (read-line (chunk-port '("aa")) 10))
(test "aa" (read-line (chunk-port '("aa\nbb")) 10))

(test-end)
