(module buffered-ports

(make-buffered-input-port)

(import chicken
        scheme)

(use posix
     data-structures
     extras
     ports
     srfi-13
     srfi-18)

(define (make-buffered-input-port port-or-fd cap)
  (##sys#check-exact cap 'make-buffered-input-port)
  (let* ((in (if (port? port-or-fd)
                 port-or-fd
                 (open-input-file* port-or-fd)))
         (fd (if (port? port-or-fd)
                 (condition-case (port->fileno in)
                   (exn (exn type) #f))
                 port-or-fd))
         (buf (make-string cap))
         (pos #f)
         (end 0)
         (fill-buffer! (lambda ()
                         (when (or (not pos)
                                   (and (fx> end 0)
                                        (fx= pos end)))
                           (if fd
                               (begin
                                 (thread-wait-for-i/o! fd #:input)
                                 (let ((x (file-read fd cap buf)))
                                   (set! end (cadr x))
                                   (set! pos 0)))
                               (begin
                                 (peek-char in)
                                 (let* ((str (read-buffered in))
                                        (len (string-length str)))
                                   ;; Some ports (e.g. those of the
                                   ;; `unix-sockets` egg) don't
                                   ;; implement `read-buffered` and
                                   ;; will always return an empty
                                   ;; string here even after
                                   ;; `peek-char` returned a
                                   ;; char. That's why we can just
                                   ;; replace `buf` with `str` in the
                                   ;; else branch, assuming that
                                   ;; `read-buffered` will
                                   ;; consistently behave like
                                   ;; that. In case `peek-char`
                                   ;; returned #!eof, `read-string!`
                                   ;; will return 0 and thus make this
                                   ;; port produce #!eof, too.
                                   (if (fx= 0 len)
                                       (begin
                                         (set! end (read-string! 1 buf in))
                                         (set! pos 0))
                                       (begin
                                         (set! buf str)
                                         (set! end len)
                                         (set! pos 0)))))))))
         (read-buffer! (lambda (max)
                         (let ((old-pos pos))
                           (set! pos (if (fx< (fx- end pos) max)
                                         end
                                         (fx+ pos max)))
                           (substring buf old-pos pos))))
         (%read-char (lambda ()
                       (fill-buffer!)
                       (if (zero? end)
                           #!eof
                           (let ((char (string-ref buf pos)))
                             (set! pos (fx+ pos 1))
                             char))))
         (%char-ready? (lambda ()
                         (or (and pos (fx< pos end))
                             (char-ready? in))))
         (%close (lambda ()
                   (close-input-port in)
                   (set! pos 0)
                   (set! end 0)
                   (set! buf #f)))
         (%peek-char (lambda ()
                       (fill-buffer!)
                       (if (fx= 0 end)
                           #!eof
                           (string-ref buf pos))))
         (%read-string! (lambda (port num out offset)
                          (when num (##sys#check-exact num 'read-string!))
                          (when offset (##sys#check-exact offset 'read-string!))
                          (let loop ((rem num)
                                     (offset offset))
                            (if (fx= 0 rem)
                                num
                                (begin
                                  (fill-buffer!)
                                  (if (fx= 0 end)
                                      (fx- num rem)
                                      (let* ((str (read-buffer! rem))
                                             (len (string-length str)))
                                        (string-copy! out offset str)
                                        (loop (fx- rem len)
                                              (fx+ offset len)))))))))
         (%read-line (lambda (port max)
                       (when max (##sys#check-exact max 'read-line))
                       (let loop ((result '())
                                  (rem max))
                         (if (or (and pos (fx= 0 end))
                                 (and rem (fx= 0 rem)))
                             (reverse-string-append result)
                             (begin
                               (fill-buffer!)
                               (let* ((idx (substring-index "\n" buf pos))
                                      (new-pos (if idx (fx+ idx 1) end))
                                      (diff (fx- new-pos pos))
                                      (maxed? (and rem (fx> diff rem)))
                                      (new-pos (if maxed?
                                                   (fx+ pos rem)
                                                   new-pos))
                                      (new-rem (if (or idx maxed?)
                                                   0
                                                   (and rem (fx- rem diff))))
                                      (old-pos pos))
                                 (set! pos new-pos)
                                 (loop (cons (substring buf old-pos (if maxed? pos (or idx pos)))
                                             result)
                                       new-rem)))))))
         (%read-buffered (lambda (port)
                           (cond ((not pos) "")
                                 ((fx= pos end) "")
                                 (else
                                  (let ((old-pos pos))
                                    (set! pos end)
                                    (substring buf old-pos end)))))))
    (make-input-port %read-char
                     %char-ready?
                     %close
                     %peek-char
                     %read-string!
                     %read-line
                     %read-buffered)))

)
